module.exports = function() {
    var client = './src/client';
    var clientApp = client + '/app';
    var server = './src/server';
    var temp = './.tmp';
    var config = {
        /**
         * File Paths
         */

        temp: temp,
        index: client + '/index.html',
        client: client,
        css: temp + '/main.css',
        fonts: './bower_components/font-awesome/fonts/**/*.*',
        images: client + '/images/**/*',
        htmlTemplates: clientApp + '/**/*.html',
        js: [
            clientApp + '/**/*.module.js',
            clientApp + '/**/*.js',
            '!' + clientApp + '/**/*.spec.js'
        ],
        alljs: [
            './src/**/*.js',
            './*.js'
        ],
        allsass: [
            client + '/styles/**/*.scss'
        ],
        server: server,
        build: './dest',
        /**
         * Bower
         */
        bower: {
            json: require('./bower.json'),
            directory: './bower_components/',
            ignorePath: '../..'
        },

        /**
         * Template Cache
         */

        templateCache: {
            files: 'templates.js',
            options: {
                module: 'app.core',
                standAlone: false,
                root: 'app/'
            }
        },

        /**
         * Node
         */
        defaultPort: 8000,
        nodeServer: './src/server/app.js'

    };

    config.getWiredepDefaultOptions = function() {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };

    return config;

};
