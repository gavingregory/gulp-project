var gulp = require('gulp');
var config = require('./gulp.config')();
var args = require('yargs').argv;

var browserSync = require('browser-sync');

var port = process.env.PORT || config.defaultPort;

var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('help', $.taskListing);

gulp.task('default', ['help']);

gulp.task('lint', function() {
    return gulp.src(config.alljs)
    .pipe($.if(args.verbose, $.print()))
    .pipe($.jscs())
    .pipe($.jscsStylish())
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
    .pipe($.jshint.reporter('fail'));
});

gulp.task('clean-styles', function(done) {
    return gulp.src(config.temp + '/**/*.css')
    .pipe($.clean(done));
});

gulp.task('clean-fonts', function(done) {
    return gulp.src(config.build + '/fonts/**/*.*')
    .pipe($.clean(done));
});

gulp.task('clean-images', function(done) {
    return gulp.src(config.build + '/images/**/*.*')
    .pipe($.clean(done));
});

gulp.task('clean-code', function(done) {
    var files = [].concat(
        config.temp + '/**/*.js',
        config.build + '/**/*.html',
        config.build + '/js/**/*.js'
    );
    return gulp.src(files)
    .pipe($.clean(done));
});

gulp.task('template-cache', ['clean-code'], function() {
    return gulp.src(config.htmlTemplates)
    .pipe($.minifyHtml({ empty: true }))
    .pipe($.angularTemplatecache(
        config.templateCache.file,
        config.templateCache.options
    ))
    .pipe(gulp.dest(config.temp));
});

gulp.task('clean', ['clean-styles', 'clean-fonts', 'clean-images', 'clean-code1']);

gulp.task('styles', ['clean-styles'], function() {
    return gulp.src(config.allsass)
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
    .pipe(gulp.dest(config.temp));
});

gulp.task('fonts', ['clean-fonts'], function() {
    return gulp.src(config.fonts)
    .pipe(gulp.dest(config.build + '/fonts'));
});

gulp.task('images', ['clean-images'], function() {
    return gulp.src(config.images)
    .pipe($.imagemin({optimizationLevel: 4}))
    .pipe(gulp.dest(config.build + '/images'));
});

gulp.task('styles:watch', function() {
    gulp.watch([config.allsass], ['styles']);
});

gulp.task('wiredep', function() {
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;
    return gulp.src(config.index)
    .pipe(wiredep(options))
    .pipe($.inject(gulp.src(config.js)))
    .pipe(gulp.dest(config.client));
});

gulp.task('inject', ['wiredep', 'styles'], function() {
    return gulp.src(config.index)
    .pipe($.inject(gulp.src(config.css)))
    .pipe(gulp.dest(config.client));
});

gulp.task('serve-dev', ['inject'], function() {
    var isDev = true;
    var nodeOptions = {
        script: config.nodeServer,
        delayTime: 1,
        env: {
            'PORT': port,
            'NODE_ENV': isDev ? 'dev' : 'build'
        },
        watch: config.server
    };
    return $.nodemon(nodeOptions)
        .on('restart', ['lint'], function(ev) {
            console.log('** Nodemon restarted..');
            console.log('Files changed on restart: \n' + ev);
        }).on('start', function() {
            startBrowserSync();
            console.log('** Nodemon started..');
        }).on('crash', function() {
            console.log('** Nodemon crashed..');
        }).on('exit', function() {
            console.log('** Nodemon exited..');
        });
});

//////////////

function startBrowserSync() {
    if (browserSync.active()) {
        return;
    }

    var options = {
        proxy: 'localhost:' + port,
        port: 3000,
        files: [config.client + '**/*.*'],
        ghostNode: {
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    };
    browserSync(options);

}
